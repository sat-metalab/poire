/* global test expect */

import { render, within } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'

import React from 'react'
import App from '@components/App'

test('Renders application', () => {
  const { container } = render(<App />)
  const Navigation = container.querySelector('#Navigation')

  const settingsTab = within(Navigation).getByText(/Settings/i)
  const dacTab = within(Navigation).getByText(/DAC Tester/i)
  const playgroundTab = within(Navigation).getByText(/Playground/i)

  expect(settingsTab).toBeInTheDocument()
  expect(dacTab).toBeInTheDocument()
  expect(playgroundTab).toBeInTheDocument()
})
