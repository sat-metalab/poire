/* global describe it expect jest beforeEach afterEach */

import populateStores from '~/src/populateStores.js'
import { MockedSocket } from '~/test/fixtures.js'
import { LOG } from '@stores/TesterStore'

describe('TesterStore', () => {
  let socketStore, testerStore

  const CHANNEL_NUM = 1
  const TEST_ERROR = new Error('TEST')

  beforeEach(() => {
    ({ socketStore, testerStore } = populateStores())
  })

  describe('handleSocketChange', () => {
    it('should handle the socket change when the active socket is set', () => {
      testerStore.handleSocketChange = jest.fn()
      socketStore.setActiveSocket(new MockedSocket())
      expect(testerStore.handleSocketChange).toHaveBeenCalled()
    })

    it('should configure the testApi when the socket is active', () => {
      expect(testerStore.testAPI).toEqual(null)
      testerStore.handleSocketChange(new MockedSocket())
      expect(testerStore.testAPI).not.toEqual(null)
    })
  })

  describe('applyTestStart', () => {
    beforeEach(() => {
      socketStore.setActiveSocket(new MockedSocket())
      testerStore.testAPI.testDacChannel = jest.fn()
      LOG.error = jest.fn()
    })

    it('should start the DAC test with white noise by default', async () => {
      await testerStore.applyTestStart(CHANNEL_NUM)

      expect(testerStore.testAPI.testDacChannel).toHaveBeenCalledWith(
        CHANNEL_NUM,
        'whitenoise',
        {}
      )
    })

    it('should start the DAC test with sine and its frequency', async () => {
      testerStore.updateTesterConfig({ testSynth: 'sine' })
      await testerStore.applyTestStart(CHANNEL_NUM)

      expect(testerStore.testAPI.testDacChannel).toHaveBeenCalledWith(
        CHANNEL_NUM,
        'sine',
        { frequency: testerStore.dacConfig.frequency }
      )
    })

    it('should set the testing channel', async () => {
      await testerStore.applyTestStart(CHANNEL_NUM)
      expect(testerStore.testingChannel).toEqual(CHANNEL_NUM)
    })

    it('should log an error when the request failed', async () => {
      testerStore.testAPI.testDacChannel = jest.fn().mockRejectedValue(TEST_ERROR)
      await testerStore.applyTestStart(CHANNEL_NUM)
      expect(LOG.error).toHaveBeenCalled()
    })

    it('should reset the testing channel when the request failed', async () => {
      testerStore.testAPI.testDacChannel = jest.fn().mockRejectedValue(TEST_ERROR)
      await testerStore.applyTestStart(CHANNEL_NUM)
      expect(testerStore.testingChannel).toBeNull()
    })
  })

  describe('applyTestStop', () => {
    beforeEach(() => {
      socketStore.setActiveSocket(new MockedSocket())
      testerStore.testAPI.stopDacChannel = jest.fn()
      LOG.error = jest.fn()
    })

    it('should stop the DAC test', async () => {
      await testerStore.applyTestStop(CHANNEL_NUM)
      expect(testerStore.testAPI.stopDacChannel).toHaveBeenCalledWith(CHANNEL_NUM)
    })

    it('should reset the testing channel', async () => {
      testerStore.setTestingChannel(CHANNEL_NUM)
      await testerStore.applyTestStop(CHANNEL_NUM)
      expect(testerStore.testingChannel).toBeNull()
    })

    it('should log an error when the request failed', async () => {
      testerStore.testAPI.stopDacChannel = jest.fn().mockRejectedValue(TEST_ERROR)
      await testerStore.applyTestStop(CHANNEL_NUM)
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('applyTestTimeout', () => {
    beforeEach(() => {
      testerStore.applyTestStop = jest.fn()
      jest.useFakeTimers()
    })

    afterEach(() => {
      jest.useRealTimers()
    })

    it('should stop the test when the timout is triggered', () => {
      testerStore.applyTestTimeout(CHANNEL_NUM, testerStore.dacConfig.testInterval)
      jest.runAllTimers()
      expect(testerStore.applyTestStop).toHaveBeenCalledWith(CHANNEL_NUM)
    })
  })

  describe('applyDacTest', () => {
    beforeEach(() => {
      testerStore.applyTestStart = jest.fn()
      testerStore.applyTestTimeout = jest.fn()
    })

    it('should start the DAC test', async () => {
      await testerStore.applyDacTest(CHANNEL_NUM)
      expect(testerStore.applyTestStart).toHaveBeenCalledWith(CHANNEL_NUM)
    })

    it('should set timeout on the DAC test', async () => {
      await testerStore.applyDacTest(CHANNEL_NUM)
      expect(testerStore.applyTestTimeout).toHaveBeenCalledWith(CHANNEL_NUM, testerStore.dacConfig.testInterval)
    })
  })

  describe('applyDacCycle', () => {
    const CHANNELS = [CHANNEL_NUM, CHANNEL_NUM, CHANNEL_NUM]

    beforeEach(() => {
      testerStore.applyDacTest = jest.fn()
    })

    it('should apply the DAC test for each channel', async () => {
      await testerStore.applyDacCycle(CHANNELS)
      expect(testerStore.applyDacTest).toHaveBeenCalledTimes(CHANNELS.length)
    })
  })
})
