/* global jest */

/**
 * Mock a Socket from Socket.IO
 * @see {@link https://socket.io/docs/v4/client-socket-instance/index.html|Socket.IO client API}
 * @returns {Object} The mocked Socket.IO API
 */
function MockedSocket () {
  return {
    emit: jest.fn(),
    io: {
      on: jest.fn()
    },
    on: jest.fn()
  }
}

export {
  MockedSocket
}
