/* eslint-disable react/prop-types */
import React from 'react'
import { render } from '@testing-library/react'
import { SatieProvider } from '@stores'

import { AppStoresContext } from '@components/App'
import populateStores from '~/src/populateStores.js'

const getContextProvider = (options) => ({ children }) => (
  <SatieProvider>
    <AppStoresContext.Provider value={options?.stores || populateStores()}>
      {children}
    </AppStoresContext.Provider>
  </SatieProvider>
)

const renderWithContext = (ui, options) => render(ui, {
  wrapper: getContextProvider(options),
  ...options
})

export {
  getContextProvider,
  renderWithContext
}
