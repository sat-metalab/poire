import React, { useContext } from 'react'
import { observer } from 'mobx-react-lite'

import { AppStoresContext } from '@components/App'

import { Common } from '@sat-valorisation/ui-components'

const { Button } = Common

/**
 * ChannelButton represents an audio channel
 * @selector `.ChannelButton`
 * @memberof module:components/panels.ChannelButton
 * @returns {external:react/Component} A wrapped button
 */
const ChannelButton = observer(({ channelNum = 0 }) => {
  const { testerStore } = useContext(AppStoresContext)

  return (
    <Button
      className='ChannelButton'
      shape='circle'
      outlined={testerStore.testingChannel !== channelNum}
      type='primary'
      onClick={() => testerStore.applyDacTest(channelNum)}
    >
      {channelNum}
    </Button>
  )
})

export default ChannelButton
