import React, { useContext } from 'react'
import { observer } from 'mobx-react-lite'

import { AppStoresContext } from '@components/App'
import { AVAILABLE_TEST_SYNTH } from '@stores/TesterStore.js'

import { Inputs } from '@sat-valorisation/ui-components'
const { Field, InputNumber, Select } = Inputs

/**
 * Select that chooses the test synth of the DAC tester
 * @returns {external:mobx-react/ObserverComponent} A select
 */
const TestSynthSelect = observer(() => {
  const { testerStore } = useContext(AppStoresContext)

  return (
    <Field
      title='Test Synth'
      description='Choose the signal type to use for the test'
    >
      <Select
        options={AVAILABLE_TEST_SYNTH}
        selected={testerStore.currentTestSynth}
        onSelection={(label) => {
          const synth = AVAILABLE_TEST_SYNTH.find(format => format.label === label)

          if (synth) {
            testerStore.updateTesterConfig({ testSynth: synth.value })
          }
        }}
      />
    </Field>
  )
})

/**
 * Input that changes the duration of the test for each channel
 * @returns {external:mobx-react/ObserverComponent} A number input
 */
const TestIntervalInput = observer(() => {
  const { testerStore } = useContext(AppStoresContext)
  const value = testerStore.dacConfig.testInterval

  const onChange = testInterval => {
    if (testInterval !== value) {
      testerStore.updateTesterConfig({ testInterval })
    }
  }

  return (
    <Field
      title='Test interval'
      description='Configure the duration of the test'
    >
      <InputNumber
        min={100}
        max={5000}
        step={100}
        value={value}
        onChange={onChange}
      />
    </Field>
  )
})

/**
 * Input that changes the frequency of the test signal
 * @returns {external:mobx-react/ObserverComponent} A number input
 */
const FrequencyInput = observer(() => {
  const { testerStore } = useContext(AppStoresContext)
  const value = testerStore.dacConfig.frequency

  const onChange = frequency => {
    if (frequency !== value) {
      testerStore.updateTesterConfig({ frequency })
    }
  }

  return (
    <Field
      title='Signal frequency'
      description='Configure the frequency of the sine test signal'
    >
      <InputNumber
        min={1}
        max={5000}
        step={100}
        value={value}
        onChange={onChange}
      />
    </Field>
  )
})

export {
  TestSynthSelect,
  TestIntervalInput,
  FrequencyInput
}
