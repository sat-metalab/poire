import React, { useState, useContext } from 'react'
import { observer } from 'mobx-react-lite'

import { AppStoresContext } from '@components/App'
import { AVAILABLE_LISTENING_FORMATS } from '@stores/ConfigStore.js'

import { Inputs } from '@sat-valorisation/ui-components'
const { Field, InputText, InputNumber, Switch, Select } = Inputs

/**
 * Input that changes the name of the OSC server
 * @returns {external:mobx-react/ObserverComponent} A text input
 */
const ServerNameInput = observer(() => {
  const { configStore, statusStore } = useContext(AppStoresContext)
  const value = configStore.satieConfig.server.name || ''
  const [editedValue, editValue] = useState(value)

  const onChange = () => {
    if (editedValue !== value) {
      configStore.updateSatieConfig({
        server: {
          name: editedValue,
          supernova: configStore.satieConfig.server.supernova
        }
      })
    }
  }

  return (
    <Field
      disabled={!statusStore.editable}
      title='SuperCollider Server'
    >
      <InputText
        value={editedValue}
        placeholder='Input a name'
        onChange={e => editValue(e.target.value)}
        onBlur={onChange}
        onPressEnter={onChange}
      />
    </Field>
  )
})

/**
 * Switch that enables the use of the Supernova server
 * @returns {external:mobx-react/ObserverComponent} A switch
 */
const SupernovaSwitch = observer(() => {
  const { configStore, statusStore } = useContext(AppStoresContext)
  const value = configStore.satieConfig.server.supernova

  return (
    <Field
      disabled={!statusStore.editable}
      title='Use Supernova Server'
    >
      <Switch
        checked={configStore.satieConfig.server.supernova}
        onChange={() => {
          configStore.updateSatieConfig({
            server: {
              name: configStore.satieConfig.server.name,
              supernova: !value
            }
          })
        }}
      />
    </Field>
  )
})

/**
 * Select that chooses the listening format of SATIE
 * @todo This must be a multiple selection
 * @returns {external:mobx-react/ObserverComponent} A select
 */
const ListeningFormatSelect = observer(() => {
  const { configStore, statusStore } = useContext(AppStoresContext)

  return (
    <Field
      disabled={!statusStore.editable}
      title='Listening format'
      description='Only one format at once is supported'
    >
      <Select
        options={AVAILABLE_LISTENING_FORMATS}
        selected={configStore.currentListeningFormat}
        onSelection={(label) => {
          const format = AVAILABLE_LISTENING_FORMATS.find(format => format.label === label)

          if (format) {
            configStore.updateSatieConfig({
              listeningFormat: [format.value]
            })
          }
        }}
      />
    </Field>
  )
})

/**
 * Input that changes the output channel index for the selected listening format
 * @todo This must be mapped on each selected listening formats
 * @returns {external:mobx-react/ObserverComponent} A number input
 */
const OutBusIndexInput = observer(() => {
  const { configStore, statusStore } = useContext(AppStoresContext)
  const value = configStore.satieConfig.outBusIndex[0]

  const onChange = newValue => {
    if (newValue !== value) {
      configStore.updateSatieConfig({
        outBusIndex: [newValue]
      })
    }
  }

  return (
    <Field
      disabled={!statusStore.editable}
      title='Output channel offset'
    >
      <InputNumber
        min={0}
        value={value}
        onChange={onChange}
      />
    </Field>
  )
})

/**
 * Input that controls the number of auxiliary channels
 * @returns {external:mobx-react/ObserverComponent} A number input
 */
const NumAudioAuxInput = observer(() => {
  const { configStore, statusStore } = useContext(AppStoresContext)
  const value = configStore.satieConfig.numAudioAux

  const onChange = newValue => {
    if (newValue !== value) {
      configStore.updateSatieConfig({
        numAudioAux: newValue
      })
    }
  }

  return (
    <Field
      disabled={!statusStore.editable}
      title='Number of auxiliary channels'
    >
      <InputNumber
        min={0}
        value={value}
        onChange={onChange}
      />
    </Field>
  )
})

/**
 * Input that controls an ambisonic orders
 * @todo Implement a multi-input that adds new ambisonic order in a list
 * @returns {external:mobx-react/ObserverComponent} A number input
 */
const AmbiOrdersInput = observer(() => {
  const { configStore, statusStore } = useContext(AppStoresContext)
  const value = configStore.satieConfig.ambiOrders

  const onChange = newValue => {
    if (newValue !== value) {
      configStore.updateSatieConfig({
        ambiOrders: [newValue]
      })
    }
  }

  return (
    <Field
      disabled={!statusStore.editable}
      title='Ambisonic Orders'
      description='Only one order is supported'
    >
      <InputNumber
        min={0}
        max={3}
        value={value}
        onChange={onChange}
      />
    </Field>
  )
})

/**
 * Input that controls the minimum number of output channels
 * @returns {external:mobx-react/ObserverComponent} A number input
 */
const MinOutputBusChannelsInput = observer(() => {
  const { configStore, statusStore } = useContext(AppStoresContext)
  const value = configStore.satieConfig.minOutputBusChannels

  const onChange = newValue => {
    if (newValue !== value) {
      configStore.updateSatieConfig({
        minOutputBusChannels: newValue
      })
    }
  }

  return (
    <Field
      disabled={!statusStore.editable}
      title='Number of server outputs channels'
    >
      <InputNumber
        min={0}
        value={value}
        onChange={onChange}
      />
    </Field>
  )
})

export {
  ServerNameInput,
  SupernovaSwitch,
  ListeningFormatSelect,
  OutBusIndexInput,
  NumAudioAuxInput,
  MinOutputBusChannelsInput,
  AmbiOrdersInput
}
