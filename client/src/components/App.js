import React, { useEffect, createContext, useContext } from 'react'
import { observer } from 'mobx-react-lite'

import Sidebar from '@components/bars/Sidebar'

import Settings from '@components/pages/Settings'
import Playground from '@components/pages/Playground'
import DacTester from '@components/pages/DacTester'

import populateStores from '~/src/populateStores.js'
import PageEnum from '@models/PageEnum'

import { SatieProvider } from '@stores'

import '@styles/App.scss'
import '@sat-valorisation/ui-components/ui-components.css'

import { Context } from '@sat-valorisation/ui-components'
const { ThemeProvider } = Context

/**
 * @constant {external:react/Context} AppStoresContext - Dispatches all the App stores
 * @memberof components.App
 */
export const AppStoresContext = createContext({})

/**
 * Main window of the whole application
 * @returns {external:mobx-react/ObserverComponent} The main window
 */
const MainWindow = observer(() => {
  const { pageStore } = useContext(AppStoresContext)

  return (
    <div id='MainWindow'>
      {pageStore.activePage === PageEnum.SETTINGS && (
        <Settings />
      )}
      {pageStore.activePage === PageEnum.DAC_TESTER && (
        <DacTester />
      )}
      {pageStore.activePage === PageEnum.PLAYGROUND && (
        <Playground />
      )}
    </div>
  )
})

/**
 * The entry point of the application UI
 * @returns {external:react/Component} The webapp
 */
function App () {
  const stores = populateStores()

  useEffect(() => {
    stores.socketStore.applyConnection()

    return () => {
      stores.socketStore.applyDisconnection()
    }
  })

  return (
    <SatieProvider>
      <ThemeProvider value='simon'>
        <AppStoresContext.Provider value={stores}>
          <main className='layout'>
            <Sidebar />
            <MainWindow />
          </main>
        </AppStoresContext.Provider>
      </ThemeProvider>
    </SatieProvider>
  )
}

export default App
export { MainWindow }
