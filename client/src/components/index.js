/**
 * A React component written with JSX
 * @external react/Component
 * @see [React component documentation]{@link https://reactjs.org/docs/components-and-props.html}
 */

/**
 * A Mobx observer component that reacts to each of its properties notifying an update
 * @external mobx-react/ObserverComponent
 * @see [Mobx's React guide]{@link https://mobx.js.org/react-integration.html}
 */

/**
 * @module components/common
 * @desc Define all common components
 */

/**
 * @module components/panels
 * @desc Define all panels components
 */

/**
 * @module components/pages
 * @desc Define all page components
 */

/**
 * @module components/bars
 * @desc Define all bar components
 */
