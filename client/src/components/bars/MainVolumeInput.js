import React, { useContext } from 'react'
import { observer } from 'mobx-react-lite'

import { AppStoresContext } from '@components/App'

import '@styles/bars/MainVolumeInput.scss'

/**
 * Sets and displays the main volume of the server.
 * @memberof module:components/bars
 * Should import the socket and create an event listener in back end
 * @selector `#MainVolumeInput`
 * @returns {external:react/Component} The main volume input
 */
const MainVolumeInput = observer(() => {
  const { rendererStore } = useContext(AppStoresContext)

  return (
    <div id='MainVolumeInput'>
      <label>
        Main Volume: {rendererStore.outputDB} dB
      </label>
      <div>
        <input
          name='volume'
          className='main-vol'
          type='range'
          min='-90'
          max='6'
          step='1'
          value={rendererStore.outputDB}
          onChange={e => rendererStore.applyOutputDB(e.target.value)}
        />
      </div>
    </div>
  )
})

export default MainVolumeInput
