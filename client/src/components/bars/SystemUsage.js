import React from 'react'

import '@styles/bars/SystemUsage.scss'

/**
 * Display the usage of some metrics
 * @memberof module:components/bars.SystemUsage
 * @selector `.Usage`
 * @param {string} label - Label of the metric
 * @param {string|number} value - Value of the metric
 * @returns {external:react/Component} A metric usage
 */
function Usage ({ label = '', value = '' }) {
  return (
    <div className='Usage'>
      <div>
        {label.toUpperCase()}
      </div>
      <div>
        {value}%
      </div>
    </div>
  )
}

/**
 * Display the PySATIE server's memory and cpu usage.
 * @memberof module:components/bars
 * @selector `#SystemUsage`
 * @param {number} [memory=0] - Value of the memory usage
 * @param {number} [value=0] - Value of the CPU usage
 * @returns {external:react/Component} All system usages
 */
function SystemUsage ({ memory = 0, cpu = 0 }) {
  return (
    <div id='SystemUsage'>
      <Usage
        label='memory'
        value={memory}
      />
      <Usage
        label='cpu'
        value={cpu}
      />
    </div>
  )
}

export default SystemUsage
