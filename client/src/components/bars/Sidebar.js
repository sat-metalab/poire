import React, { useContext } from 'react'
import { observer } from 'mobx-react-lite'

import { AppStoresContext } from '@components/App'

import Navigation from '@components/bars/Navigation'
import SystemUsage from '@components/bars/SystemUsage'
import MainVolumeInput from '@components/bars/MainVolumeInput'
import SatieStatus from '@components/bars/SatieStatus'
import PowerButton from '@components/buttons/PowerButton'

import PoireLogo from '@assets/svg/poire_logo.png'

import '@styles/bars/SideBar.scss'

/**
 * Poire's Sidebar. It contains the logo, navigation bar and status
 * @memberof module:components/bars
 * @selector `#Sidebar`
 * @returns {external:react/Component} The side bar
 */
const Sidebar = observer(() => {
  const { statusStore } = useContext(AppStoresContext)

  return (
    <section id='Sidebar'>
      <div className='logo-container'>
        <img className='logo' src={PoireLogo} alt='poire logo' />
      </div>
      <Navigation />
      <div className='sidebar--info'>
        <div>
          <PowerButton />
        </div>
        {statusStore.connected && <MainVolumeInput />}
        <SatieStatus />
        <SystemUsage />
      </div>
    </section>
  )
})

export default Sidebar
