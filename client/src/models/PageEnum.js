/**
 * Enum for all page ids
 * @readonly
 * @memberof models
 * @enum {string}
 */
const PageEnum = Object.freeze({
  SETTINGS: 'settings',
  DAC_TESTER: 'dacTester',
  PLAYGROUND: 'playground'
})

export default PageEnum
