import SocketStore from '@stores/SocketStore'
import RendererStore from '@stores/RendererStore'
import StatusStore from '@stores/StatusStore'
import PageStore from '@stores/PageStore'
import ConfigStore from '@stores/ConfigStore'
import TesterStore from '@stores/TesterStore'
import SourceStore from '@stores/SourceStore'
import PluginStore from '@stores/PluginStore'

/**
 * Creates all the app's Stores
 * @returns {object} Object with all stores used by Poire
 */
function populateStores () {
  const socketStore = new SocketStore()
  const pageStore = new PageStore()
  const statusStore = new StatusStore(socketStore)
  const configStore = new ConfigStore(socketStore)

  const rendererStore = new RendererStore(socketStore)
  const testerStore = new TesterStore(socketStore)
  const sourceStore = new SourceStore(socketStore)
  const pluginStore = new PluginStore(socketStore)

  return {
    socketStore,
    pageStore,
    statusStore,
    rendererStore,
    configStore,
    testerStore,
    sourceStore,
    pluginStore
  }
}

export default populateStores
