import API from '@api/API'

/**
 * @classdesc API that manages the Satie renderer
 * @memberof module:api
 * @extends module:api.API
 */
class RendererAPI extends API {
  /**
   * Set the output DB of the SATIE renderer
   * @param {number} outputVolumeDB - DB of the renderer output
   * @async
   */
  setOutputDB (outputVolumeDB) {
    return new Promise((resolve, reject) => {
      this.socket.emit('/satie/renderer/setOutputDB', outputVolumeDB, (error, success) => {
        if (error) {
          reject(error)
        } else {
          resolve(success)
        }
      })
    })
  }

  /**
   * Listens to all changes of the outputDB
   * @param {Function} onChangeAction - Function triggered when the output DB is changed
   */
  onOutputDBChanged (onChangeAction) {
    this.socket.on('/satie/renderer/outputDB/changed', outputDB => {
      onChangeAction(outputDB)
    })
  }
}

export default RendererAPI
