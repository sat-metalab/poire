import API from '@api/API'

/**
 * @classdesc API that modifies each instanciated Nodes
 * @memberof module:api
 * @extends module:api.API
 */
class NodeAPI extends API {
  /**
   * Query the instanciated audio sources
   * @async
   */
  querySourceNodes () {
    return new Promise((resolve, reject) => {
      this.socket.emit('/satie/scene/nodes/query', (error, success) => {
        if (error) {
          reject(error)
        } else {
          resolve(success)
        }
      })
    })
  }

  /**
   * Send request to create a new sources node
   * @async
   */
  createSource (source) {
    return new Promise((resolve, reject) => {
      this.socket.emit('/satie/scene/createSource', source, (error, success) => {
        if (error) {
          reject(error)
        } else {
          resolve(success)
        }
      })
    })
  }

  /**
   * Send request to delete a sources node
   * @async
   */
  deleteSource (source) {
    return new Promise((resolve, reject) => {
      this.socket.emit('/satie/scene/deleteSource', source, (error, success) => {
        if (error) {
          reject(error)
        } else {
          resolve(success)
        }
      })
    })
  }

  /**
   * Send request to update a basic propertie sources node
   * @async
   */
  updateSource (source) {
    return new Promise((resolve, reject) => {
      this.socket.emit('/satie/scene/updateSource', source, (error, success) => {
        if (error) {
          reject(error)
        } else {
          resolve(success)
        }
      })
    })
  }

  /**
   * Send request to update a synthdef properties of a sources node
   * @async
   */
  updateSynthdefSource (source) {
    return new Promise((resolve, reject) => {
      this.socket.emit('/satie/scene/updateSynthdefSource', source, (error, success) => {
        if (error) {
          reject(error)
        } else {
          resolve(success)
        }
      })
    })
  }

  /**
   * Listen to all source node creations
   * @async
   */
  onConfirmationCreation (onConfirmationAction) {
    this.socket.on('/satie/plugins/confirmationCreation', (quidId) => {
      onConfirmationAction(quidId)
    })
  }
}

export default NodeAPI
