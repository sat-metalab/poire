/* global location */

import { makeObservable, observable, action, computed } from 'mobx'

import io from 'socket.io-client'
import logger from '@utils/logger'

/**
 * @constant {external:pino/logger} LOG - Dedicated logger for the SocketStore
 * @private
 */
export const LOG = logger.child({ store: 'SocketStore' })

/**
 * @constant {number} SOCKET_RECONNECTION_DELAY - Time to wait before attempting a new reconnection in milliseconds
 * @memberof SocketStore
 */
const SOCKET_RECONNECTION_DELAY = 100

/**
 * @constant {number} SOCKET_RECONNECTION_DELAY_MAX - Maximum amount of time to wait between reconnections in milliseconds
 * @memberof SocketStore
 */
const SOCKET_RECONNECTION_DELAY_MAX = 5000

/**
 * @constant {number} SOCKET_RECONNECTION_ATTEMPTS - Number of reconnection attempts to execute
 * @memberof SocketStore
 */
const SOCKET_RECONNECTION_ATTEMPTS = 1

/** Stores the active socket and its configuration */
class SocketStore {
  /**
   * @property {string} [host=localhost] - URL or IP address of the Poire server.
   * The default value is localhost.
   */
  host = location.hostname

  /** @property {string} [port=5000] - Port of the Poire server. */
  port = 5000

  /** @property {?external:socketIO/Socket} [activeSocket] - Current socket communicating with the Poire server. */
  activeSocket = null

  constructor () {
    makeObservable(this, {
      host: observable,
      port: observable,
      activeSocket: observable,
      setActiveSocket: action,
      hasActiveSocket: computed,
      setEndpoint: action
    })
  }

  /** @property {string} [endpoint=localhost:5000] - Endpoint of the Poire server. */
  get endpoint () {
    let endpoint = `${this.host}:${this.port}`

    const currentURL = new URL(window.location)
    const urlParameters = currentURL.searchParams
    const urlEndpoint = urlParameters.get('endpoint')

    if (urlEndpoint) {
      endpoint = urlEndpoint
    }

    return endpoint
  }

  /**
   * Checks if the active socket exists
   * @returns {boolean} Returns true if the active socket isn't null
   */
  get hasActiveSocket () {
    return this.activeSocket !== null
  }

  /**
   * Handles the WebSocket connection
   * @param {external:socketIO/Socket} connectedSocket - The connected socket
   */
  handleSocketConnection (connectedSocket) {
    LOG.info(`Socket is connected with ${this.endpoint}`)
    this.setActiveSocket(connectedSocket)

    connectedSocket.on('disconnect', () => this.handleSocketDisconnection())
  }

  /** Handles the WebSocket disconnection */
  handleSocketDisconnection () {
    LOG.info(`Socket is disconnected from ${this.endpoint}`)
    this.setActiveSocket(null)
  }

  /**
   * Tries to establish a Websocket connection with a Poire server.
   * @see https://socket.io/docs/client-connection-lifecycle/
   * @param {external:socketIO/Socket} socket - Custom socket (used for tests)
   */
  applyConnection (socket) {
    if (this.hasActiveSocket) {
      this.setActiveSocket(null)
    }

    const candidateSocket = socket ?? io(this.endpoint, {
      reconnectionDelay: SOCKET_RECONNECTION_DELAY,
      reconnectionDelayMax: SOCKET_RECONNECTION_DELAY_MAX,
      reconnectionAttempts: SOCKET_RECONNECTION_ATTEMPTS
    })

    candidateSocket.io.on('reconnect_failed', () => {
      LOG.info(`Failed to connect with ${this.host}:${this.port}`)
    })

    candidateSocket.on('connect', () => this.handleSocketConnection(candidateSocket))
  }

  /** Cuts the Websocket connection with the Poire server */
  applyDisconnection () {
    if (this.activeSocket) {
      this.activeSocket.disconnect()
    }
  }

  /**
   * Sets a new socket as the active socket
   * @param {external:socketIO/Socket} socket - A new candidate socket
   */
  setActiveSocket (socket) {
    this.activeSocket = socket
  }

  /**
   * Configures a new host and a new port for the Poire server
   * @param {string} host - Host of the Poire server
   * @param {string} port - Websocket port of the Poire server
   */
  setEndpoint (host, port) {
    this.host = host
    this.port = port
  }
}

export default SocketStore
