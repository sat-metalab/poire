import React from 'react'
import SatieStatus from '../components/Sidebar/SatieStatus'

export default {
  title: 'Sidebar/SatieStatus',
  component: SatieStatus
}

export const SatieStatusStatic = () => <SatieStatus />
