import React from 'react'
import MainVolume from '../components/Sidebar/MainVolume'

export default {
  title: 'Sidebar/MainVolume',
  component: MainVolume
}

export const MainVolumeBox = () => <MainVolume />
